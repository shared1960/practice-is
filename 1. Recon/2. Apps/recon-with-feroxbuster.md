# Feroxbuster

Feroxbuster is a powerful and efficient web directory and file enumeration tool that is designed to help security testers and web developers discover hidden directories and files on web servers. It is written in Rust programming language, which makes it fast and efficient. Feroxbuster uses various techniques to find files and directories, including brute-forcing, recursive crawling, and wordlist-based discovery. It also allows users to specify various options, such as the number of threads, wordlists, user agents, cookies, and more. Overall, Feroxbuster is a highly recommended tool for anyone who needs to perform web directory and file enumeration tasks.


1. *Feroxbuster* dapat digunakan secara sederhana dengan menggunakan perintah :

``` bash
feroxbuster -u http://target.com
```

2. Mengkombinasikan dengan wordlist

```bash
feroxbuster -u http://target.com -w lokasi/wordlist.txt
```

3. Menentukan jumlah threads yang digunakan untuk memproses fuzzing

```bash
feroxbuster -u http://target.com -t 10
```

4. Menentukan User-Agent dari fuzzer

```bash
feroxbuster -u http://target.com -H "User-Agent: Google/bot"
```

5. Menyertakan Cookies pada fuzzer

```bash
feroxbuster -u http://target.com -C "phpsessid=192921hf34jsvjsnjs"
```

6. Memfilter status code yang ditampilkan

```bash
feroxbuster -u http://target.com --filter-status 404
```

7. Menurunkan tingkat keamanan koneksi

```bash
feroxbuster -u https://target.com --insecure
```