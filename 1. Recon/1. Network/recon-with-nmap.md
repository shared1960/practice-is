
## Nmap

### Tujuan

Modul ini bertujuan untuk memberikan pemahaman mendalam tentang penggunaan Nmap dalam pengujian penetrasi. Peserta akan belajar tentang berbagai fitur Nmap, teknik pemindaian, identifikasi kerentanan, dan penggunaan output untuk analisis lebih lanjut.

### Persyaratan

- Pengetahuan dasar tentang jaringan komputer dan protokol TCP/IP.
- Akses ke mesin target untuk latihan.
- Instalasi Nmap di komputer yang digunakan oleh peserta.

#### Apa itu Nmap ?

Nmap (Network Mapper) adalah sebuah perangkat lunak sumber terbuka yang dirancang untuk melakukan pemindaian jaringan dan analisis keamanan. Ini merupakan alat yang kuat dan serbaguna yang digunakan oleh para profesional keamanan, administrator jaringan, dan peneliti untuk memeriksa keamanan jaringan, menemukan host dan layanan dalam jaringan, serta mengidentifikasi kerentanan potensial.

#### Mengapa Nmap itu penting dalam Pentest?

1. Nmap digunakan untuk pemindaian jaringan.
2. Melakukan identifikasi layanan dan port yang terbuka.
3. Melakukan pemindaian versi parang perangkat lunak.
4. Memiliki fitur NSE (Nmap Scripting Engine) yang memungkinkan pengguna untuk membuat script dalam melakukan tugas khusus.
5. Melakukan analisa keamanan.
6. Melakukan pengujian keamanan.

#### Instalasi Nmap

**Windows**

1. Unduh Nmap:
	- Kunjungi situs web resmi Nmap: https://nmap.org/download.html
	- Pilih versi instalator yang sesuai dengan sistem operasi Windows Anda (biasanya berkas .exe).
	- Unduh berkas instalator dan simpan di lokasi yang mudah diakses pada komputer Anda.
2. Instalasi:
	- Buka berkas instalator yang telah Anda unduh.
	- Ikuti petunjuk instalasi yang diberikan oleh proses instalasi.
	- Pilih lokasi instalasi dan konfigurasi sesuai kebutuhan Anda.
	- Lanjutkan hingga instalasi selesai.
3. Verifikasi Instalasi:
	- Buka Command Prompt (cmd).
	- Ketik `nmap -V` dan tekan Enter.
	- Jika instalasi berhasil, versi Nmap yang terinstal akan ditampilkan.

**Linux (Debian)**

1. Menggunakan APT (Advanced Package Tool):
	- Buka terminal.
	- Jalankan perintah berikut untuk memperbarui daftar paket:
	
```bash 
sudo apt update
```

- Setelah pembaruan selesai, jalankan perintah berikut untuk menginstal Nmap:

``` bash
sudo apt install nmap
```

- Ketikkan kata sandi pengguna (root) jika diminta, lalu tunggu hingga proses instalasi selesai.

2. Verifikasi Instalasi:
	- Setelah instalasi selesai, Anda dapat memverifikasi Nmap dengan menjalankan perintah:
	
```bash
	nmap -v
```
	
- Outputnya harus menampilkan versi Nmap yang terinstal.


#### Pemindaian

##### Pemindaian TCP SYN

Pemindaian TCP SYN sering digunakan untuk memeriksa port yang terbuka di tujuan. Ini adalah salah satu teknik pemindaian yang paling cepat dan paling ringan.

``` bash
nmap -sS target_ip
```

Contoh :

``` bash
nmap -sS 192.168.1.1
```

##### Pemindaian TCP Connect

Pemindaian TCP Connect membuat koneksi lengkap ke port yang dituju. Ini sering digunakan karena lebih andal dan kurang terdeteksi oleh firewall.

``` bash
nmap -sT target_ip
```

Contoh :

``` bash
nmap -sT 192.168.1.1
```

##### Pemindaian UDP

Pemindaian UDP digunakan untuk menemukan layanan UDP yang berjalan di tujuan.

``` bash
nmap -sU target_ip
```

Contoh :

``` bash
nmap -sU 192.168.1.1
```

##### Pemindaian Versi Layanan

Pemindaian versi layanan digunakan untuk mengidentifikasi versi layanan yang berjalan di port tertentu.

``` bash
nmap -sV target_ip
```

Contoh :

``` bash
nmap -sV 192.168.1.1
```

##### Pemindaian Subnet dan Range IP

Anda juga dapat melakukan pemindaian pada subnets atau rentang alamat IP dengan menentukan subnet atau rentang IP yang diinginkan.

``` bash
nmap -sS target_ip/subnet
```

Contoh : 

```bash
nmap -sS 192.168.1.0/24
```

##### Pemindaian dengan NSE

1. Mendeteksi kerentanan HTTP pada Apache Struts
```bash
   nmap --script http-vuln-cve2017-5638.nse target_ip
```
2. Mengumpulkan informasi HTTP
``` bash
nmap --script http-title.nse target_ip
```
3. Mendeteksi Backdoor
``` bash
nmap --script backdoor.nse target_ip
```
4. Menjalankan semua script NSE
``` bash
nmap --script all target_ip
```

#### Analisa output Nmap

``` bash
Starting Nmap 7.91 ( https://nmap.org ) at 2024-03-19 12:00 UTC
Nmap scan report for target_host (192.168.1.100)
Host is up (0.0010s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE       VERSION
22/tcp   open  ssh           OpenSSH 7.6p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http          Apache httpd 2.4.29 ((Ubuntu))
111/tcp  open  rpcbind       2-4 (RPC #100000)
445/tcp  open  microsoft-ds  Microsoft Windows Server 2019 Standard microsoft-ds
8080/tcp open  http-proxy    Squid proxy 3.5.27
```

##### Interpretasi Hasil

1. **Informasi Target :**
	- Alamat IP target adalah 192.168.1.100.
	- Host tersebut dinyatakan 'up' (tersedia).
2. **Informasi Port :**
	- Terdapat beberapa port terbuka, seperti port 22 (SSH), 80 (HTTP), 111 (RPCBIND), 445 (Microsoft-DS), dan 8080 (HTTP-Proxy).
	- Versi layanan juga ditampilkan, seperti versi OpenSSH 7.6p1, Apache httpd 2.4.29, dan Squid proxy 3.5.27.
	- Port yang tidak ditampilkan (998 closed ports) menunjukkan bahwa tidak ada layanan yang aktif di port tersebut.
3. **Deteksi Kerentanan :**
	- Tidak ada deteksi kerentanan spesifik yang ditampilkan dalam contoh ini, tetapi Nmap dapat menampilkan deteksi kerentanan jika ditemukan.

``` bash
nmap --script http-sql-injection.nse target_ip
```

``` bash
Starting Nmap 7.91 ( https://nmap.org ) at 2024-03-19 12:00 UTC
Nmap scan report for target_host (192.168.1.100)
Host is up (0.0010s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE
80/tcp open  http
| http-sql-injection:
|   VULNERABLE:
|   SQL injection
|     State: VULNERABLE
|     Description:
|       The script tried to use an SQL injection attack against the target, but was unable to confirm the vulnerability.
|     References:
|       - https://example.com/sql-injection
```

##### Intepretasi Hasil :

1. **Informasi Target**:
    
    - Alamat IP target adalah 192.168.1.100.
    - Host tersebut dinyatakan 'up' (tersedia).
2. **Informasi Port**:
    
    - Terdapat satu port terbuka, yaitu port 80 (HTTP).
3. **Deteksi Kerentanan SQL Injection**:
    
    - Skrip NSE `http-sql-injection` mencoba mendeteksi kerentanan SQL injection pada server web yang berjalan di port 80.
    - Hasil pemindaian menunjukkan bahwa kerentanan SQL injection mungkin ada di server web tersebut.
    - Namun, skrip tidak dapat mengonfirmasi kerentanan karena alasan tertentu.